'''HI'''
# pylint: disable=R0903
from Crypto.Cipher import AES
from chef.exceptions import ChefError
import base64
import chef
import hashlib
import simplejson as json
import os
# This class is a wrapper over pychef library. It implements encrypted data
# bags, which are not present in the current version of pychef. Additionaly it
# helps initialize pychef from /etc/chef with default client.rb created by
# chef-client.


class PyChef(object):
    ''' class '''
    def __init__(self, secret):
        '''init methode'''
         # Pychef requires client.rb to contain key_path parameter,
        # even if it is set to default value.
        temp1 = os.path.isfile("/etc/chef/client.rb")
        if os.path.isdir("/etc/chef") and temp1:
            has_key_path = False
            for line in open("/etc/chef/client.rb"):
                if not line.strip() or line.startswith('#'):
                    continue
                key = line.split(None, 1)[0]
                if key == 'client_key':
                    has_key_path = True
                    break

            if not has_key_path:
                with open("/etc/chef/client.rb", "a") as client_rb:
                    client_rb.write("\nclient_key '/etc/chef/client.pem'\n")

        self.api = chef.autoconfigure()
        self.encrypted_secret = secret

    # Returns data bag item as a dict
    def encrypted_data_bag_item(self, data_bag_name, item_name):
        '''encrypted_data_bag_item.'''
        enc_data_bag = PyChef.EncryptedDataBagItem(data_bag_name, item_name,
                                                   self.encrypted_secret)
        return dict((key, enc_data_bag[key]) for key in enc_data_bag)

    # Returns environment data in form of dictionary
    @classmethod
    def environment(cls):
        '''environment'''
        #env = chef.Environment(name)
        return {"default": None,
                "override": None,
                "cookbook_versions": None,
                "description": None}

    class ChefUnsupportedEncryptionVersionError(ChefError):
        '''ChefUnsupportedEncryptionVersionError'''
        def __init__(self, version):
            '''init'''
            message = "This version of chef does not support encrypted data"\
                      + "bag item format version %s" % version
            return ChefError.__init__(self, message)

    class ChefDecryptionError(ChefError):
        """Error decrypting data bag value. Most likely the provided key is
        incorrect"""

    class EncryptedDataBagItem(chef.DataBagItem):
        '''EncryptedDataBagItem.'''
        SUPPORTED_ENCRYPTION_VERSIONS = (1,)

        def __init__(self, api=None, skip_load=False, **kwqargs):
            '''init'''
            self.encryption_key = open(kwqargs['secret_file']).read().strip()
            super(PyChef.EncryptedDataBagItem, self).__init__(kwqargs['bag'],
                                                              kwqargs['name'],
                                                              api=api,
                                                              skip_load=
                                                              skip_load)

        def __getitem__(self, key):
            '''getitem.'''
            if key == 'id':
                return self.raw_data[key]
            else:
                return PyChef.EncryptedDataBagItem.Decryptors.create_decryptor(
                    self.encryption_key, self.raw_data[key]).decrypt()

        @staticmethod
        def get_version(data):
            '''get_version'''
            temp = PyChef.EncryptedDataBagItem.SUPPORTED_ENCRYPTION_VERSIONS
            if 'version' in data.keys():
                if data['version'] in temp:
                    return data['version']
                else:
                    raise PyChef.ChefUnsupportedEncryptionVersionError(
                        data['version'])
            else:
                # Should be 0 after implementing DecryptorVersion0
                return "1"

        class Decryptors(object):
            '''Decryptors'''
            STRIP_CHARS = map(chr, range(0, 31))

            def __init__(self):
                pass

            @staticmethod
            def create_decryptor(encryption_key, data):
                '''create_decryptor'''
                tep = PyChef.EncryptedDataBagItem.Decryptors.DecryptorVersion1(
                    encryption_key, data['encrypted_data'], data['iv'])
                return {
                    1: tep}[PyChef.EncryptedDataBagItem.get_version(data)]

            class DecryptorVersion1(object):
                '''DecryptorVersion1'''
                AES_MODE = AES.MODE_CBC

                def __init__(self, encryption_key, data, iv_name):
                    '''init'''
                    self.encryption_key = hashlib.sha256(
                        encryption_key).digest()
                    self.data = base64.standard_b64decode(data)
                    self.iv_name = base64.standard_b64decode(iv_name)
                    self.decryptor = AES.new(self.encryption_key,
                                             self.AES_MODE, self.iv_name)

                def decrypt(self):
                    '''decrypt.'''
                    value = self.decryptor.decrypt(self.data)
                    # Strip all the \r and \n characters
                    value = value.strip(reduce(lambda x, y: "%s%s" % (x, y),
                                        PyChef.EncryptedDataBagItem.Decryptors.
                                        STRIP_CHARS))
                    # After decryption we should get a JSON string
                    try:
                        value = json.loads(value)
                    except ValueError:
                        raise PyChef.ChefDecryptionError()
                    return value['json_wrapper']
