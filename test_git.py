'''Testcases related to git hub.'''
# pylint: disable=R0904
import unittest
#import mock
from mock import patch
import schl_utils.git


class GitTestCase(unittest.TestCase):
    '''Tesing for checkout,fetch,clone work proper.'''
    @patch('subprocess.Popen')
    def test_checkout(self, mockpopen):
        '''Testing for checkout files from git.'''
        schl_utils.git.checkout('some/path', 'some_ref')

        self.assertEqual(mockpopen.call_count, 1)
        #temp=(posargs,keyargs)
        posargs, keyargs = mockpopen.call_args
        self.assertTrue('some_ref' in posargs[0])
        self.assertEqual(keyargs['cwd'], 'some/path')

    @patch('subprocess.Popen')
    def test_fetch(self, mockpopen):
        '''Testing for fetch files from git.'''
        schl_utils.git.fetch('some/path')

        self.assertEqual(mockpopen.call_count, 1)
        #temp=(posargs,keyargs)
        temp = mockpopen.call_args
        self.assertEqual(temp[1]['cwd'], 'some/path')

    @patch('subprocess.Popen')
    def test_clone(self, mockpopen):
        '''Testing for clone . '''
        schl_utils.git.clone('some/path', 'repo.address')

        self.assertEqual(mockpopen.call_count, 1)
        temp = mockpopen.call_args
        self.assertTrue('repo.address' in temp[0][0])
