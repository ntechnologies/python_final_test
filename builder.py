'''This module contains webassets static content generator library'''
from webassets import Bundle
import os.path
from jinja2 import Environment as Jinja2Environment
from webassets import Environment as AssetsEnvironment
from webassets.ext.jinja2 import AssetsExtension
from webassets.filter.jinja2 import Jinja2

# Wrapper class for webassets static content generator library
# Implements custom configuration parsing (as read from yml)
# See README for more information on configuration format


class Builder(object):
    '''This is Wrapper class '''
    def __init__(self, config, url, src_path, dst_path):
        self.config = config
        self.src_path = src_path
        self.dst_path = dst_path
        self.env = AssetsEnvironment(directory=dst_path,
                                     url=url, load_path=[src_path])
        jinja2_env = Jinja2Environment(extensions=[AssetsExtension])
        jinja2_env.assets_environment = self.env

        # This monkey patch is here to supply Jinja2 environment to filter
        #for webassets
        # This issue will be solved in webassets 0.9, it should be removed
        # after updating version
        def output_with_env(self, _in, out):
            '''This supplies Jinja2 environment'''
            out.write(jinja2_env.from_string(_in.read(),
                                             template_class=
                                             self.jinja2.Template).
                      render(self.context or {}))
            Jinja2.output = output_with_env

    def parse_bundle(self, name, outname, path, filters, *content):
        '''This will parse bundle attributes'''
        outpath = '%s/%s' % (path, outname)
        bundle = Bundle(*content, output=outpath, filters=filters)
        bundle_name = '%s/%s' % (path, name)
        self.env.register(bundle_name, bundle)
        output = self.env[bundle_name].urls()
        print "Processing bundle: %s, output: %s" % (bundle_name, output[0])
        for fill in content:
            if type(fill) is Bundle:
                print "  Included file (template): %s" % fill.contents[0]
            else:
                print "  Included file: %s" % fill

    def bundle_jinja(self, path):
        '''This function will return a new
        bundle with jinja2'''
        bundle = Bundle(path, filters='jinja2')
        self.env.register(path, bundle)
        print "Processing template: %s" % path
        return bundle

    def parse_content(self, content):
        '''This function looks for key jinja
        and appends to list'''
        output = []
        for el_fill in content:
            if type(el_fill) is dict:
                if el_fill['type'] == 'jinja':
                    output.append(self.bundle_jinja(el_fill['name']))
                else:
                    output.append(el_fill['name'])
            else:
                output.append(el_fill)
        return output

    def bundle_directory(self, outname, cache, content):
        '''This method processes directory in bundle contain '''
        for directory in content:
            print "Processing directory: %s" % directory
            src_dir = '%s/%s' % (self.src_path, directory)
            for temp in os.walk(src_dir):
                for el_fill in temp[2]:
                    rel_path = os.path.relpath(temp[0], src_dir)
                    bundle_name = os.path.normpath(
                        '%s/%s/%s' % (outname, rel_path, el_fill))

                    if cache:
                        cache_busted_filepath = '%s/%s/%s-%%(version)s.%s' % (
                            (outname, rel_path) + tuple(el_fill.split('.')))
                        outpath = cache_busted_filepath
                    else:
                        outpath = bundle_name

                    filepath = '%s/%s/%s' % (directory, rel_path, el_fill)
                    bundle = Bundle(filepath, output=outpath)
                    self.env.register(bundle_name, bundle)
                    output = self.env[bundle_name].urls()
                    print "  Processing file: %s, output: %s"\
                        % (os.path.normpath(filepath), output[0])

    def build(self):
        '''This is used to build bundle'''
        for bundle in self.config:
            content = self.parse_content(bundle['content'])
            if bundle['type'] == 'js' or bundle['type'] == 'css' or \
                    bundle['type'] == 'plain':
                if bundle['cache_buster']:
                    outname = '%s-%%(version)s.%s' % tuple(
                        bundle['name'].split('.'))
                else:
                    outname = bundle['name']

                if bundle['type'] == 'js':
                    filters = ('jsmin')
                elif bundle['type'] == 'css':
                    filters = ('cssmin')
                else:
                    filters = ()

                self.parse_bundle(bundle['name'], outname,
                                  bundle['output_path'], content, filters)

            elif bundle['type'] == 'directory':
                self.bundle_directory(bundle['output_path'],
                                      content, bundle['cache_buster'])
            else:
                print "Unrecognized bundle type: %s" % bundle
