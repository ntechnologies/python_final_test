''' Use for upload on server.'''
import schl_utils.git as git
from schl_utils.s3 import S3Uploader
from schl_utils.chef import ChefHelpers
from schl_utils.assets import Builder
from yaml import load
import argparse
import os.path
import os
import shutil

# Argument parsing, specific for this script
INPUT_PATH = None
PARSER = argparse.ArgumentParser()
PARSER.add_argument('CONFIG_file',
                    help='Yaml file with the CONFIGuration data.')
PARSER.add_argument('environment',
                    help=('Enviroment designation. Should match '
                          'to the chef environment, e.g. dev/qa/prod.'))
PARSER.add_argument('--input_dir',
                    help=('Path to the directory with input files. '
                          'If git_tag is present, directory where the '
                          'git repo will be checked out. '
                          '(default: proj/src/PROJECT_NAME)'),
                    default='proj/src/_PROJECT_NAME')
PARSER.add_argument('--output_dir',
                    help=('Path to the directory with output files for '
                          'builder. If git_tag is present, directory '
                          'where the repo content will be copied before '
                          'uploading (default: proj/output/PROJECT_NAME)'),
                    default='proj/output/_PROJECT_NAME')
PARSER.add_argument('--secret_file',
                    help=('Path to the file containing Chef data bag '
                          'encryption key. '
                          '(default: /etc/chef/encrypted_data_bag_secret)'),
                    default='/etc/chef/encrypted_data_bag_secret')
PARSER.add_argument('--git_tag',
                    help='Git tag to deploy', default=None)
PARSER.add_argument('--force_clone',
                    help='Force git clone', action='store_true')
ARGS = PARSER.parse_args()

# Load yml CONFIGuration file
CONFIG_FILE = ARGS.CONFIG_file

CONFIG = load(open(CONFIG_FILE, "r"))

# Combine CONFIGuration with command line options
CONFIG['git_tag'] = ARGS.git_tag
CONFIG['env'] = ARGS.environment

# Get information from Chef
CHEF_HELPER = ChefHelpers(ARGS.secret_file)
S3_CRED = CHEF_HELPER.get_S3_CREDentials(CONFIG['aws_credentials_data_bag'],
                                         ARGS.environment)
BUCKET = CHEF_HELPER.get_s3_bucket(CONFIG['project'], ARGS.environment)

# If default parameters are used for input or output directories,
# generate them using project name
INPUT_DIR = ARGS.input_dir.replace('_PROJECT_NAME', CONFIG['project'])
OUTPUT_DIR = ARGS.output_dir.replace('_PROJECT_NAME', CONFIG['project'])

# Make sure that every directory on path to input and output directories exists
for (directory, print_type) in [(INPUT_DIR, 'input'), (OUTPUT_DIR, 'output')]:
    path = ''
    for subdir in directory.split('/'):
        path += subdir + '/'
        if subdir != '' and not os.path.isdir(path):
            print "Creating directory for %s: %s" % (print_type, path)
            os.makedirs(path)

# If git_tag option is set, get the latest data from git into input directory
# Additionaly, if force_clone is set, make sure that input_directory is purged
if CONFIG['git_tag']:
    if not os.path.isdir(INPUT_DIR) or ARGS.force_clone:
        if ARGS.force_clone:
            print "Forcing clone..."
            shutil.rmtree(INPUT_PATH)
        git.clone(INPUT_DIR, CONFIG['git_repo'])
    else:
        git.fetch(INPUT_DIR)

    git.checkout(INPUT_DIR, CONFIG['git_tag'])

# Three workflow paths
# 1. with building assets; builder used to generate output
# 2. without building, with git; output is copied from input
# 3. without git or building; input directory is used as output directory
#                             for purpose of uploading to S3
if 'build' in CONFIG:
    # Workflow 1
    # Builder requires dictionary parsed from yml as its CONFIGuration
    # Additional attributes are
    # - url to be used as a prefix for generated files
    # - input directory with sources
    # - output directory to which build output is generated
    URL = '/%s/' % CONFIG['project']
    BUILDER = Builder(CONFIG['build'], URL, INPUT_DIR, OUTPUT_DIR)
    print "Project %s requires build" % CONFIG['project']
    BUILDER.build()
else:
    if CONFIG['git_tag']:
        # Workflow 2
        # Make sure directory is cleared before copying file so that no old
        # versions of files are uploaded
        print "Project %s does not require build," +\
              "copying sources from git repo to output" % CONFIG['project']
        if os.path.isdir(INPUT_DIR):
            shutil.rmtree(OUTPUT_DIR)
        shutil.copytree(INPUT_DIR, OUTPUT_DIR)
    else:
        # Workflow 3
        # Input and output directory are the same
        TEMP1 = 'does not require build and does not use git,uploading'\
                + ' directly from'
        print "Project %s %s %s" % (CONFIG['project'], TEMP1, INPUT_DIR)
        OUTPUT_PATH = INPUT_PATH

# Prepare CONFIGuration for AWS
CONFIG['aws'] = {}
CONFIG['aws']['access_key'] = S3_CRED['access_key']
CONFIG['aws']['secret_key'] = S3_CRED['secret_key']

# Use schl_utils wrapper to upload to S3. Arguments:
# - aws credentials: dictionary with access_key and secret_key
# - name of the bucket to which the files will be uploaded to
# - input directory for uploader, which in this script is output directory
#   of builder
S3UPLOADER = S3Uploader(CONFIG['aws'], BUCKET, OUTPUT_DIR)
print 'Uploading files to bucket %s...' % S3UPLOADER.BUCKET.name

# Upload function only uploads files which changed, based on MD5 checksum
# Returns number of files actually uploaded
COUNT = S3UPLOADER.upload_files()
print 'Uploaded %s file(s).' % COUNT
