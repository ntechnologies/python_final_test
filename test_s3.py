'''Testcases for file upload on amazon.'''
# pylint: disable=R0904

import unittest
import mock
from mock import patch
from schl_utils.s3 import S3Uploader
import boto.s3.bucket

MOCK_AWS_CONFIG = {"access_key": "key", "secret_key": "key"}


class S3TestCase(unittest.TestCase):
    '''Testcase for setup and upload file on amazon .'''
    @patch('boto.s3.connection.S3Connection')
    def setUp(self, mocks3connection):
        '''Testcase for proper connection .'''
        super(S3TestCase, self).setUp()
        self.mock_connection_class = mocks3connection
        self.mock_connection = mocks3connection.return_value
        self.mock_bucket = mock.MagicMock(spec=boto.s3.bucket.Bucket)
        self.mock_connection.get_bucket.return_value = self.mock_bucket
        self.uploader = S3Uploader(
            MOCK_AWS_CONFIG,
            "bucket_name",
            "/file/path")

    def tearDown(self):
        pass

    def test_upload_files(self):
        '''Testcase for file upload on amazon.'''
        self.uploader.upload_files()
#        from nose.tools import set_trace; set_trace()
        self.mock_connection_class.assert_called_with(
            MOCK_AWS_CONFIG['access_key'],
            MOCK_AWS_CONFIG['secret_key'])
        self.mock_connection.get_bucket.assert_called_with("bucket_name")
