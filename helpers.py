'''This module implements specific use cases of
Chef connecvity based on underlying pychef library'''
import schl_utils.chef
# This class implements specific use cases of Chef connecvity based on
# underlying pychef library.Any new functionality should be added here.
# It should utilize PyChef as a wrapper over actual pychef library.


class ChefHelpers(object):
    '''This class contains helper function'''
    def __init__(self, data_bag_secret='/etc/chef/encrypted_data_bag_secret'):
        self.chef = schl_utils.chef.PyChef(data_bag_secret)

    # Returns s3 credentials in form of dictionary
    # with access_key and secret_key
    def get_s3_credentials(self, data_bag_name, env):
        '''This returns s3 credentials'''
        data = self.chef.encrypted_data_bag_item(data_bag_name, env)
        return {'access_key': data['access_key'],
                'secret_key': data['secret_key']}

    # Returns bucket name for desired service as a string
    # Todo add logic for override attributes
    def get_s3_bucket(self, service_name, env):
        '''This is returns bucket name for desired service as a string '''
        return self.chef.environment(env)['default']['services'][
            's3_bucket'][service_name]
